//
//  File.swift
//  
//
//  Created by admin on 8/3/23.
//


import Foundation
import WebKit
import AVFoundation

class EngageWebviewController : UIViewController, WKUIDelegate {
    var webView: WKWebView!
    var matchId = ""
    var accessKey = ""
    var secretKey = ""
    //var arBaseUrl = "https://icc.onrender.com"
//    var arBaseUrl = "https://iccwebar.fanisko.com:1234"
    var arBaseUrl = "https://webar.fanisko.com/index.html"
    
    override func viewDidLoad() {
       super.viewDidLoad()
        
        view.backgroundColor = .white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(self.backToInitial(sender:)))
        
 //       self.showLoading()
 //       self.validateUser()
        
         requestPermission()
    }
    
    func requestPermission(){
        print("Requesting permission")
        AVCaptureDevice.requestAccess(for: .video) { success in
             if success { // if request is granted (success is true)
                 print("Pemission granted")
                 DispatchQueue.main.async {
 //                    self.showLoading()
 //                    self.validateUser()
                     
                         let myURL = URL(string: "\(self.arBaseUrl)?match_id=\(self.matchId)")
                         let myRequest = URLRequest(url: myURL!)
                         self.webView.load(myRequest)
                     
                 }
             } else { // if request is denied (success is false)
               // Create Alert
                 print("Pemission not granted")
                 DispatchQueue.main.async {
                     
                     if self.webView != nil { // Make sure the view exists
                         
                         if self.view.subviews.contains(self.webView) {
                             self.webView.removeFromSuperview() // Remove it
                         } else {
                                 // Do Nothing
                         }
                     }
                     
                         //let errorView = UIView(frame: .zero)
                     let errorView = UIView()
                     errorView.frame = CGRect(x: 0, y:0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                     errorView.backgroundColor = self.hexStringToUIColor(hex: "#310173")
                     
                     let imageName = "errorViewCamIcon"
                     let path = Bundle.module.path(forResource: imageName, ofType: "png") ?? ""
                     let image = UIImage(contentsOfFile: path) ?? UIImage()
                     let imageView = UIImageView(image: image)
                     imageView.frame = CGRect(x: 0, y: 200, width: 200, height: 200)
                     imageView.center.x = self.view.center.x
                     errorView.addSubview(imageView)
                     
                     let enableCameraLabel = UILabel()
                     enableCameraLabel.frame = CGRect(x: 0, y: imageView.frame.maxY+60, width: UIScreen.main.bounds.width, height: 20)
                     enableCameraLabel.text = "Enable Camera"
                     enableCameraLabel.textAlignment = .center
                     enableCameraLabel.numberOfLines = 0
                     enableCameraLabel.font = .boldSystemFont(ofSize: 24)
                     enableCameraLabel.textColor = .white
                     
                     errorView.addSubview(enableCameraLabel)
                     
                     let enableCameraDescLabel = UILabel()
                     enableCameraDescLabel.frame = CGRect(x: 60, y: enableCameraLabel.frame.maxY+10, width: UIScreen.main.bounds.width-120, height: 80)
                     enableCameraDescLabel.text = "Please provide access to your camera, which is required for Augmented Reality"
                     enableCameraDescLabel.textAlignment = .center
                     enableCameraDescLabel.numberOfLines = 0
                     enableCameraDescLabel.font = .systemFont(ofSize: 18)
                     enableCameraDescLabel.textColor = .white
                     
                     errorView.addSubview(enableCameraDescLabel)
                     
                     let button = UIButton(type: .roundedRect)
                     button.frame = CGRect(x: 0, y: enableCameraDescLabel.frame.maxY+80, width: 200, height: 50)
                     button.center.x = self.view.center.x
                     button.layer.cornerRadius = 25; // this value vary as per your desire
                     button.clipsToBounds = true;
                     button.tintColor = .black
                     button.backgroundColor = .white
                     button.setTitle("Go to settings".uppercased(), for: .normal)
                     button.addTarget(self, action:#selector(self.goToSettings), for: .touchUpInside)
                     
                     errorView.addSubview(button)
                     
                     let borderImageName = "errorViewBorderIcon"
                     let borderImagePath = Bundle.module.path(forResource: borderImageName, ofType: "png") ?? ""
                     let borderImage = UIImage(contentsOfFile: borderImagePath) ?? UIImage()
                     let borderImageView = UIImageView(image: borderImage)
                     borderImageView.frame = CGRect(x: UIScreen.main.bounds.width-350, y: UIScreen.main.bounds.height-350, width: 350, height: 350)
                     errorView.addSubview(borderImageView)
                     errorView.sendSubviewToBack(borderImageView)
                     
                     self.view.addSubview(errorView)
                 }
             }
        }
    }
    
    @objc func goToSettings(){
        print("Going to Settings")
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
    
    
    @objc func backToInitial(sender: AnyObject) {
        print("Custom Back")
        if(webView.canGoBack) {
            print("Have history")
            webView.goBack()
        } else {
            print("No history")
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    func validateUser(){
        let parameters: [String: Any] = ["access_key": accessKey, "secret_key": secretKey]
        
        let url = URL(string: "https://webview.fanisko.com/validation/")!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // add headers for the request
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request) { data, response, error in
            
            if let error = error {
                print("Post Request Error: \(error.localizedDescription)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode)
            else {
                print("Invalid Response received from the server")
                return
            }
            
            guard let responseData = data else {
                print("nil Data received from the server")
                return
            }
            
            do {
                if let jsonResponse = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any] {
                    print(jsonResponse)
                    let success = jsonResponse["success"] as? Int ?? 0
                    let message = jsonResponse["message"] as? String ?? "Authorization Failed!"
                    
                    DispatchQueue.main.async {
                        self.dimissLoading()
                        
                        if(success == 1){
                            let myURL = URL(string: "\(self.arBaseUrl)?match_id=\(self.matchId)")
                            let myRequest = URLRequest(url: myURL!)
                            self.webView.load(myRequest)
                        }
                        else{
                            let alertLabel = UILabel()
                            alertLabel.text = message
                            alertLabel.textAlignment = .center
                            alertLabel.numberOfLines = 0
                            
                            self.view.addSubview(alertLabel)
                            
                            alertLabel.translatesAutoresizingMaskIntoConstraints = false
                            alertLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                            alertLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
                        }
                    }
                } else {
                    print("data maybe corrupted or in wrong format")
                    throw URLError(.badServerResponse)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    func showLoading(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func dimissLoading(){
        dismiss(animated: false, completion: nil)
    }
    
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.websiteDataStore = WKWebsiteDataStore.default()
        webConfiguration.preferences.javaScriptCanOpenWindowsAutomatically = true
        webConfiguration.allowsInlineMediaPlayback = true
        
        webConfiguration.preferences.setValue(true, forKey: "javaScriptEnabled")
        webConfiguration.preferences.setValue(true, forKey: "loadsImagesAutomatically")
        webConfiguration.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        if #available(iOS 14.0, *) {
            webConfiguration.defaultWebpagePreferences.allowsContentJavaScript = true
        } else {
            webConfiguration.preferences.javaScriptEnabled = true
        }
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        
        webView.uiDelegate = self
        view = webView
    }
     
     func hexStringToUIColor (hex:String) -> UIColor {
         var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

         if (cString.hasPrefix("#")) {
             cString.remove(at: cString.startIndex)
         }

         if ((cString.count) != 6) {
             return UIColor.gray
         }

         var rgbValue:UInt64 = 0
         Scanner(string: cString).scanHexInt64(&rgbValue)

         return UIColor(
             red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
             green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
             blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
             alpha: CGFloat(1.0)
         )
     }
 }
