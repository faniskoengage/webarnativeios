
import UIKit
import Foundation

public struct EngageWebviewiOS {
    public init() {
    }
    
    public static func getEngageWebviewViewController(matchId : String,accessKey : String, secretKey : String) -> UIViewController{
        let bundle = Bundle(for: EngageWebviewController.self)
        let vc = EngageWebviewController(nibName: "EngageWebviewController", bundle: bundle)
        //vc.titleText = titleText
        vc.matchId = matchId
        vc.accessKey = accessKey
        vc.secretKey = secretKey
        return vc
    }
}
